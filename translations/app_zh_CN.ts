<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main_window.cpp" line="44"/>
        <source>PNG Files(*.png)</source>
        <translation>PNG文件(*.png)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="45"/>
        <source>Open...</source>
        <translation>打开...</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="52"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="52"/>
        <source>Open file failed!</source>
        <translation>打开文件失败！</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="82"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="126"/>
        <source>Save...</source>
        <translation>保存...</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="148"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="149"/>
        <source>&amp;Open</source>
        <translation>打开(&amp;O)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="150"/>
        <source>&amp;Close</source>
        <translation>关闭(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="152"/>
        <source>E&amp;xit</source>
        <translation>退出(&amp;X)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="154"/>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="155"/>
        <source>&amp;About</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="184"/>
        <source>Original</source>
        <translation>原始</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="202"/>
        <source>Lighten</source>
        <translation>加亮</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="204"/>
        <source>Lighten R:</source>
        <translation>加亮R：</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="206"/>
        <source>Lighten G:</source>
        <translation>加亮G：</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="208"/>
        <source>Lighten B:</source>
        <translation>加亮B：</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="210"/>
        <location filename="../main_window.cpp" line="260"/>
        <location filename="../main_window.cpp" line="306"/>
        <location filename="../main_window.cpp" line="338"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="211"/>
        <location filename="../main_window.cpp" line="261"/>
        <location filename="../main_window.cpp" line="307"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="252"/>
        <source>Darken</source>
        <translation>调暗</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="254"/>
        <source>Darken R:</source>
        <translation>调暗R：</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="256"/>
        <source>Darken G:</source>
        <translation>调暗G：</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="258"/>
        <source>Darken B:</source>
        <translation>调暗B:</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="302"/>
        <source>Gray</source>
        <translation>灰显</translation>
    </message>
    <message>
        <location filename="../main_window.cpp" line="304"/>
        <source>Lightness:</source>
        <translation>亮度：</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../global_values.cpp" line="5"/>
        <source>Icon Assistant</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../global_values.cpp" line="6"/>
        <source>Bran Lee</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../global_values.cpp" line="7"/>
        <source>***************</source>
        <translation></translation>
    </message>
</context>
</TS>

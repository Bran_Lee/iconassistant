#include <QCoreApplication>
#include <QDir>
#include "global_values.h"

const QString global_app_name      = QObject::tr("Icon Assistant");
const QString global_app_author    = QObject::tr("Bran Lee");
const QString global_app_copyright = QObject::tr("***************");

bool initGlobals()
{
  QString app_dir = QCoreApplication::applicationDirPath() + "/";
  QDir::setCurrent(app_dir);

  return true;
}

void unInitGlobals()
{
}

QDate compilationDate()
{
  const char* const months[] =
  {
    "Null",
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };
  QString date_string = QString(__DATE__).remove(" ");

  QString month_part = date_string.left(3);
  for(int i=1; i<=12; i++)
    if(month_part == QString(months[i]))
    {
      date_string.replace(month_part, QString("%1").arg(i, 2, 10, QLatin1Char('0')));
      break;
    }

  if(date_string.size() != 8)
    date_string.insert(2, '0');

  return QDate::fromString(date_string, "MMddyyyy");
}

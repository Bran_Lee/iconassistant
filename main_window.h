#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QPixmap>

class QLabel;
class QSpinBox;
class QCheckBox;
class QPushButton;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void onOpen();
  void onClose();
  void onExit();
  void onAbout();

  void onLightenRefresh();
  void onDarkenRefresh();
  void onGrayRefresh();
  void onLightenSaveCheck();
  void onDarkenSaveCheck();
  void onGraySaveCheck();
  void onSave();

protected:
  void initMenus();
  void initCenter();
  void saveSettings();
  void restoreSettings();
  void updateWidgetsState();

  void refreshAll();
  void refreshOriginal();
  void refreshLighten();
  void refreshDarken();
  void refreshGray();

  void convertToGray(QImage &image, int lighten=0);
  void adjustLightness(QImage &image, int lr, int lg, int lb, int la=0);

  QMenu   *p_file_menu_;
  QAction *p_open_act_;
  QAction *p_close_act_;
  QAction *p_exit_act_;

  QMenu   *p_help_menu_;
  QAction *p_about_act_;

  QLabel *p_original_label_;
  QLabel *p_original_icon_;

  QLabel    *p_lighten_label_;
  QLabel    *p_lighten_icon_;
  QLabel    *p_lighten_r_label_;
  QSpinBox  *p_lighten_r_spin_;
  QLabel    *p_lighten_g_label_;
  QSpinBox  *p_lighten_g_spin_;
  QLabel    *p_lighten_b_label_;
  QSpinBox  *p_lighten_b_spin_;
  QCheckBox *p_lighten_save_check_;
  QPushButton *p_lighten_refresh_button_;

  QLabel    *p_darken_label_;
  QLabel    *p_darken_icon_;
  QLabel    *p_darken_r_label_;
  QSpinBox  *p_darken_r_spin_;
  QLabel    *p_darken_g_label_;
  QSpinBox  *p_darken_g_spin_;
  QLabel    *p_darken_b_label_;
  QSpinBox  *p_darken_b_spin_;
  QCheckBox *p_darken_save_check_;
  QPushButton *p_darken_refresh_button_;

  QLabel    *p_gray_label_;
  QLabel    *p_gray_icon_;
  QLabel    *p_gray_lightness_label_;
  QSpinBox  *p_gray_lightness_spin_;
  QCheckBox *p_gray_save_check_;
  QPushButton *p_gray_refresh_button_;

  QPushButton *p_save_button_;

  bool    b_working_;
  QImage  working_icon_;
  QString working_file_;
};

#endif // MAIN_WINDOW_H

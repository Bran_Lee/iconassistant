#ifndef GLOBAL_VALUES
#define GLOBAL_VALUES

#include <QDate>

// Version informations
#define MAJOR_VERSION   (1)
#define MINOR_VERSION   (0)
#define STAGE_VERSION   (0)
#define DATE_VERSION    (compilationDate().toString("yyMMdd"))

extern const QString global_app_name;
extern const QString global_app_author;
extern const QString global_app_copyright;

bool  initGlobals();
void  unInitGlobals();
QDate compilationDate();

#endif // GLOBAL_VALUES


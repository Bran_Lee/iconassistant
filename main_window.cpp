#include <QCoreApplication>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QKeySequence>
#include <QMessageBox>
#include <QFileDialog>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QSettings>
#include <QRegExp>
#include "main_window.h"
#include "global_values.h"

#define LIGHTEN_DEFAULT (32)
#define DARKEN_DEFAULT  (-32)
#define GRAY_LIGHTNESS  (0)

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
  setWindowTitle(QObject::tr(global_app_name.toLocal8Bit()));
  setFixedSize(640, 360);

  b_working_ = false;
  initMenus();
  initCenter();
  restoreSettings();
  updateWidgetsState();
}

MainWindow::~MainWindow()
{
  saveSettings();
}

void MainWindow::onOpen()
{
  QString filter = tr("PNG Files(*.png)");
  QString file = QFileDialog::getOpenFileName(this, tr("Open..."), QString(), filter);
  if(file.isEmpty())
    return;

  QImage open_icon(file, "png");
  if(open_icon.isNull())
  {
    QMessageBox::critical(this, tr("Error"), tr("Open file failed!"));
    return;
  }

  if(open_icon.format() != QImage::Format_ARGB32)
    open_icon = open_icon.convertToFormat(QImage::Format_ARGB32);

  b_working_    = true;
  working_icon_ = open_icon;
  working_file_ = file;
  refreshAll();
  updateWidgetsState();
  setWindowTitle(QObject::tr(global_app_name.toLocal8Bit()) + " - " + file);
}

void MainWindow::onClose()
{
  b_working_    = false;
  working_icon_ = QImage();
  working_file_ = QString();
  refreshAll();
  updateWidgetsState();
  setWindowTitle(QObject::tr(global_app_name.toLocal8Bit()));
}

void MainWindow::onExit()
{
  close();
}

void MainWindow::onAbout()
{
  QString about_title = tr("About") + " " + QObject::tr(global_app_name.toLocal8Bit());
  QString about_text  = QString("%1 V%2.%3.%4(%5)\n\n%6\n%7")
                        .arg(QObject::tr(global_app_name.toLocal8Bit()))
                        .arg(MAJOR_VERSION)
                        .arg(MINOR_VERSION)
                        .arg(STAGE_VERSION)
                        .arg(DATE_VERSION)
                        .arg(QObject::tr(global_app_author.toLocal8Bit()))
                        .arg(QObject::tr(global_app_copyright.toLocal8Bit()));
  QMessageBox::about(this, about_title, about_text);
}

void MainWindow::onLightenRefresh()
{
  refreshLighten();
}

void MainWindow::onDarkenRefresh()
{
  refreshDarken();
}

void MainWindow::onGrayRefresh()
{
  refreshGray();
}

void MainWindow::onLightenSaveCheck()
{
  updateWidgetsState();
}

void MainWindow::onDarkenSaveCheck()
{
  updateWidgetsState();
}

void MainWindow::onGraySaveCheck()
{
  updateWidgetsState();
}

void MainWindow::onSave()
{
  QString save_dir = QFileDialog::getExistingDirectory(this, tr("Save..."));
  if(save_dir.isEmpty())
    return;
  save_dir = save_dir.replace("\\", "/") + "/";

  QRegExp header_hint("(.*)([\\\\/]+)");
  QString simple_name = QString(working_file_).remove(header_hint);

  QRegExp name_hint("(.*)(\\.{1})(.*)");
  if(name_hint.exactMatch(simple_name))
  {
    if(p_lighten_save_check_->isChecked())
      p_lighten_icon_->pixmap()->save(save_dir + name_hint.cap(1) + "_lighten" + name_hint.cap(2) + name_hint.cap(3));
    if(p_darken_save_check_->isChecked())
      p_darken_icon_->pixmap()->save(save_dir + name_hint.cap(1) + "_darken" + name_hint.cap(2) + name_hint.cap(3));
    if(p_gray_save_check_->isCheckable())
      p_gray_icon_->pixmap()->save(save_dir + name_hint.cap(1) + "_gray" + name_hint.cap(2) + name_hint.cap(3));
  }
}

void MainWindow::initMenus()
{
  p_file_menu_ = menuBar()->addMenu(tr("&File"));
  p_open_act_  = p_file_menu_->addAction(tr("&Open"));
  p_close_act_ = p_file_menu_->addAction(tr("&Close"));
  p_file_menu_->addSeparator();
  p_exit_act_  = p_file_menu_->addAction(tr("E&xit"));

  p_help_menu_ = menuBar()->addMenu(tr("&Help"));
  p_about_act_ = p_help_menu_->addAction(tr("&About"));

  connect(p_open_act_, SIGNAL(triggered()), SLOT(onOpen()));
  connect(p_close_act_, SIGNAL(triggered()), SLOT(onClose()));
  connect(p_exit_act_, SIGNAL(triggered()), SLOT(onExit()));
  connect(p_about_act_, SIGNAL(triggered()), SLOT(onAbout()));

  p_open_act_->setShortcut(QKeySequence("Ctrl+O"));
  p_close_act_->setShortcut(QKeySequence("Ctrl+C"));
  p_exit_act_->setShortcut(QKeySequence("Ctrl+X"));
}

QFrame* createHLine(QWidget *parent)
{
  QFrame *p_hline = new QFrame(parent);
  p_hline->setFrameShape(QFrame::HLine);
  return p_hline;
}

QFrame* createVLine(QWidget *parent)
{
  QFrame *p_hline = new QFrame(parent);
  p_hline->setFrameShape(QFrame::VLine);
  return p_hline;
}

void MainWindow::initCenter()
{
  // original icon
  p_original_label_  = new QLabel(tr("Original"), this);
  p_original_icon_   = new QLabel(this);

  p_original_label_->setAlignment(Qt::AlignCenter);
  p_original_icon_->setFixedSize(64, 64);
  p_original_icon_->setScaledContents(true);

  QHBoxLayout *p_original_icon_layout = new QHBoxLayout();
  p_original_icon_layout->addStretch();
  p_original_icon_layout->addWidget(p_original_icon_);
  p_original_icon_layout->addStretch();

  QVBoxLayout *p_original_layout = new QVBoxLayout();
  p_original_layout->addWidget(p_original_label_);
  p_original_layout->addWidget(createHLine(this));
  p_original_layout->addLayout(p_original_icon_layout);
  p_original_layout->addWidget(createHLine(this));
  p_original_layout->addStretch();

  // lighten icon
  p_lighten_label_   = new QLabel(tr("Lighten"), this);
  p_lighten_icon_    = new QLabel(this);
  p_lighten_r_label_ = new QLabel(tr("Lighten R:"), this);
  p_lighten_r_spin_  = new QSpinBox(this);
  p_lighten_g_label_ = new QLabel(tr("Lighten G:"), this);
  p_lighten_g_spin_  = new QSpinBox(this);
  p_lighten_b_label_ = new QLabel(tr("Lighten B:"), this);
  p_lighten_b_spin_  = new QSpinBox(this);
  p_lighten_save_check_ = new QCheckBox(tr("Save"), this);
  p_lighten_refresh_button_ = new QPushButton(tr("Refresh"), this);

  connect(p_lighten_save_check_, SIGNAL(clicked()), SLOT(onLightenSaveCheck()));
  connect(p_lighten_refresh_button_, SIGNAL(clicked()), SLOT(onLightenRefresh()));

  p_lighten_label_->setAlignment(Qt::AlignCenter);
  p_lighten_icon_->setFixedSize(64, 64);
  p_lighten_icon_->setScaledContents(true);
  p_lighten_r_spin_->setRange(0, 255);
  p_lighten_g_spin_->setRange(0, 255);
  p_lighten_b_spin_->setRange(0, 255);
  p_lighten_r_spin_->setValue(LIGHTEN_DEFAULT);
  p_lighten_g_spin_->setValue(LIGHTEN_DEFAULT);
  p_lighten_b_spin_->setValue(LIGHTEN_DEFAULT);
  p_lighten_r_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_lighten_g_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_lighten_b_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_lighten_save_check_->setChecked(true);

  QHBoxLayout *p_lighten_icon_layout = new QHBoxLayout();
  p_lighten_icon_layout->addStretch();
  p_lighten_icon_layout->addWidget(p_lighten_icon_);
  p_lighten_icon_layout->addStretch();

  QGridLayout *p_lighten_config_layout = new QGridLayout();
  p_lighten_config_layout->addWidget(p_lighten_refresh_button_, 0, 0, 1, 2, Qt::AlignCenter);
  p_lighten_config_layout->addWidget(p_lighten_r_label_, 1, 0, 1, 1);
  p_lighten_config_layout->addWidget(p_lighten_r_spin_,  1, 1, 1, 1);
  p_lighten_config_layout->addWidget(p_lighten_g_label_, 2, 0, 1, 1);
  p_lighten_config_layout->addWidget(p_lighten_g_spin_,  2, 1, 1, 1);
  p_lighten_config_layout->addWidget(p_lighten_b_label_, 3, 0, 1, 1);
  p_lighten_config_layout->addWidget(p_lighten_b_spin_,  3, 1, 1, 1);

  QVBoxLayout *p_lighten_layout = new QVBoxLayout();
  p_lighten_layout->addWidget(p_lighten_label_);
  p_lighten_layout->addWidget(createHLine(this));
  p_lighten_layout->addLayout(p_lighten_icon_layout);
  p_lighten_layout->addWidget(createHLine(this));
  p_lighten_layout->addLayout(p_lighten_config_layout);
  p_lighten_layout->addStretch();
  p_lighten_layout->addWidget(p_lighten_save_check_);

  // darken icon
  p_darken_label_   = new QLabel(tr("Darken"), this);
  p_darken_icon_    = new QLabel(this);
  p_darken_r_label_ = new QLabel(tr("Darken R:"), this);
  p_darken_r_spin_  = new QSpinBox(this);
  p_darken_g_label_ = new QLabel(tr("Darken G:"), this);
  p_darken_g_spin_  = new QSpinBox(this);
  p_darken_b_label_ = new QLabel(tr("Darken B:"), this);
  p_darken_b_spin_  = new QSpinBox(this);
  p_darken_save_check_ = new QCheckBox(tr("Save"), this);
  p_darken_refresh_button_ = new QPushButton(tr("Refresh"), this);

  connect(p_darken_save_check_, SIGNAL(clicked()), SLOT(onDarkenSaveCheck()));
  connect(p_darken_refresh_button_, SIGNAL(clicked()), SLOT(onDarkenRefresh()));

  p_darken_label_->setAlignment(Qt::AlignCenter);
  p_darken_icon_->setFixedSize(64, 64);
  p_darken_icon_->setScaledContents(true);
  p_darken_r_spin_->setRange(-255, 0);
  p_darken_g_spin_->setRange(-255, 0);
  p_darken_b_spin_->setRange(-255, 0);
  p_darken_r_spin_->setValue(DARKEN_DEFAULT);
  p_darken_g_spin_->setValue(DARKEN_DEFAULT);
  p_darken_b_spin_->setValue(DARKEN_DEFAULT);
  p_darken_r_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_darken_g_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_darken_b_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_darken_save_check_->setChecked(true);

  QHBoxLayout *p_darken_icon_layout = new QHBoxLayout();
  p_darken_icon_layout->addStretch();
  p_darken_icon_layout->addWidget(p_darken_icon_);
  p_darken_icon_layout->addStretch();

  QGridLayout *p_darken_config_layout = new QGridLayout();
  p_darken_config_layout->addWidget(p_darken_refresh_button_, 0, 0, 1, 2, Qt::AlignCenter);
  p_darken_config_layout->addWidget(p_darken_r_label_, 1, 0, 1, 1);
  p_darken_config_layout->addWidget(p_darken_r_spin_,  1, 1, 1, 1);
  p_darken_config_layout->addWidget(p_darken_g_label_, 2, 0, 1, 1);
  p_darken_config_layout->addWidget(p_darken_g_spin_,  2, 1, 1, 1);
  p_darken_config_layout->addWidget(p_darken_b_label_, 3, 0, 1, 1);
  p_darken_config_layout->addWidget(p_darken_b_spin_,  3, 1, 1, 1);

  QVBoxLayout *p_darken_layout = new QVBoxLayout();
  p_darken_layout->addWidget(p_darken_label_);
  p_darken_layout->addWidget(createHLine(this));
  p_darken_layout->addLayout(p_darken_icon_layout);
  p_darken_layout->addWidget(createHLine(this));
  p_darken_layout->addLayout(p_darken_config_layout);
  p_darken_layout->addStretch();
  p_darken_layout->addWidget(p_darken_save_check_);

  // gray icon
  p_gray_label_ = new QLabel(tr("Gray"), this);
  p_gray_icon_  = new QLabel(this);
  p_gray_lightness_label_ = new QLabel(tr("Lightness:"), this);
  p_gray_lightness_spin_  = new QSpinBox(this);
  p_gray_save_check_      = new QCheckBox(tr("Save"), this);
  p_gray_refresh_button_  = new QPushButton(tr("Refresh"), this);

  connect(p_gray_save_check_, SIGNAL(clicked()), SLOT(onGraySaveCheck()));
  connect(p_gray_refresh_button_, SIGNAL(clicked()), SLOT(onGrayRefresh()));

  p_gray_label_->setAlignment(Qt::AlignCenter);
  p_gray_icon_->setFixedSize(64, 64);
  p_gray_icon_->setScaledContents(true);
  p_gray_lightness_spin_->setRange(-255, 255);
  p_gray_lightness_spin_->setValue(GRAY_LIGHTNESS);
  p_gray_lightness_spin_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  p_gray_save_check_->setChecked(true);

  QHBoxLayout *p_gray_icon_layout = new QHBoxLayout();
  p_gray_icon_layout->addStretch();
  p_gray_icon_layout->addWidget(p_gray_icon_);
  p_gray_icon_layout->addStretch();

  QGridLayout *p_gray_config_layout = new QGridLayout();
  p_gray_config_layout->addWidget(p_gray_refresh_button_,  0, 0, 1, 2, Qt::AlignCenter);
  p_gray_config_layout->addWidget(p_gray_lightness_label_, 1, 0, 1, 1);
  p_gray_config_layout->addWidget(p_gray_lightness_spin_,  1, 1, 1, 1);

  QVBoxLayout *p_gray_layout = new QVBoxLayout();
  p_gray_layout->addWidget(p_gray_label_);
  p_gray_layout->addWidget(createHLine(this));
  p_gray_layout->addLayout(p_gray_icon_layout);
  p_gray_layout->addWidget(createHLine(this));
  p_gray_layout->addLayout(p_gray_config_layout);
  p_gray_layout->addStretch();
  p_gray_layout->addWidget(p_gray_save_check_);

  // buttons
  p_save_button_ = new QPushButton(tr("Save"), this);

  connect(p_save_button_, SIGNAL(clicked()), SLOT(onSave()));

  QHBoxLayout *p_buttons_layout = new QHBoxLayout();
  p_buttons_layout->addStretch();
  p_buttons_layout->addWidget(p_save_button_);

  // combine
  QHBoxLayout *p_icons_layout = new QHBoxLayout();
  p_icons_layout->addLayout(p_original_layout);
  p_icons_layout->addWidget(createVLine(this));
  p_icons_layout->addLayout(p_lighten_layout);
  p_icons_layout->addWidget(createVLine(this));
  p_icons_layout->addLayout(p_darken_layout);
  p_icons_layout->addWidget(createVLine(this));
  p_icons_layout->addLayout(p_gray_layout);

  QWidget *p_central_widget = new QWidget(this);
  QVBoxLayout *p_main_layout = new QVBoxLayout();
  p_main_layout->addLayout(p_icons_layout);
  p_main_layout->addWidget(createHLine(this));
  p_main_layout->addLayout(p_buttons_layout);
  delete p_central_widget->layout();
  p_central_widget->setLayout(p_main_layout);
  setCentralWidget(p_central_widget);
}

void MainWindow::saveSettings()
{
  QSettings settings("config.ini", QSettings::IniFormat);

  settings.setValue("lighten_r", p_lighten_r_spin_->value());
  settings.setValue("lighten_g", p_lighten_g_spin_->value());
  settings.setValue("lighten_b", p_lighten_b_spin_->value());
  settings.setValue("lighten_save", p_lighten_save_check_->isChecked());

  settings.setValue("darken_r", p_darken_r_spin_->value());
  settings.setValue("darken_g", p_darken_g_spin_->value());
  settings.setValue("darken_b", p_darken_b_spin_->value());
  settings.setValue("darken_save", p_darken_save_check_->isChecked());

  settings.setValue("gray_lightness", p_gray_lightness_spin_->value());
  settings.setValue("gray_save", p_gray_save_check_->isChecked());
}

void MainWindow::restoreSettings()
{
  QSettings settings("config.ini", QSettings::IniFormat);

  p_lighten_r_spin_->setValue(settings.value("lighten_r", LIGHTEN_DEFAULT).toInt());
  p_lighten_g_spin_->setValue(settings.value("lighten_g", LIGHTEN_DEFAULT).toInt());
  p_lighten_b_spin_->setValue(settings.value("lighten_b", LIGHTEN_DEFAULT).toInt());
  p_lighten_save_check_->setChecked(settings.value("lighten_save", true).toBool());

  p_darken_r_spin_->setValue(settings.value("darken_r", DARKEN_DEFAULT).toInt());
  p_darken_g_spin_->setValue(settings.value("darken_g", DARKEN_DEFAULT).toInt());
  p_darken_b_spin_->setValue(settings.value("darken_b", DARKEN_DEFAULT).toInt());
  p_darken_save_check_->setChecked(settings.value("darken_save", true).toBool());

  p_gray_lightness_spin_->setValue(settings.value("gray_lightness", GRAY_LIGHTNESS).toInt());
  p_gray_save_check_->setChecked(settings.value("gray_save", true).toBool());
}

void MainWindow::updateWidgetsState()
{
  p_close_act_->setEnabled(b_working_);
  p_lighten_refresh_button_->setEnabled(b_working_);
  p_darken_refresh_button_->setEnabled(b_working_);
  p_gray_refresh_button_->setEnabled(b_working_);

  bool b_save_something = p_lighten_save_check_->isChecked() ||
                          p_darken_save_check_->isChecked()  ||
                          p_gray_save_check_->isChecked();
  p_save_button_->setEnabled(b_working_ && b_save_something);
}

void MainWindow::refreshAll()
{
  refreshOriginal();
  refreshLighten();
  refreshDarken();
  refreshGray();
}

void MainWindow::refreshOriginal()
{
  p_original_icon_->setPixmap(QPixmap());
  if(b_working_)
    p_original_icon_->setPixmap(QPixmap::fromImage(working_icon_));
}

static void correctColor(int &color)
{
  if(color<0)
    color = 0;
  else if(color>255)
    color = 255;
}

static void correctRgba(int &r, int &g, int &b, int &a)
{
  correctColor(r);
  correctColor(g);
  correctColor(b);
  correctColor(a);
}

void MainWindow::refreshLighten()
{
  p_lighten_icon_->setPixmap(QPixmap());
  if(!b_working_)
    return;

  int lighten_r = p_lighten_r_spin_->value();
  int lighten_g = p_lighten_g_spin_->value();
  int lighten_b = p_lighten_b_spin_->value();

  QImage lighten_icon = working_icon_;
  adjustLightness(lighten_icon, lighten_r, lighten_g, lighten_b);
  p_lighten_icon_->setPixmap(QPixmap::fromImage(lighten_icon));
}

void MainWindow::refreshDarken()
{
  p_darken_icon_->setPixmap(QPixmap());
  if(!b_working_)
    return;

  int darken_r = p_darken_r_spin_->value();
  int darken_g = p_darken_g_spin_->value();
  int darken_b = p_darken_b_spin_->value();

  QImage darken_icon = working_icon_;
  adjustLightness(darken_icon, darken_r, darken_g, darken_b);
  p_darken_icon_->setPixmap(QPixmap::fromImage(darken_icon));
}

void MainWindow::refreshGray()
{
  p_gray_icon_->setPixmap(QPixmap());
  if(!b_working_)
    return;

  int gray_lightness = p_gray_lightness_spin_->value();

  QImage gray_icon = working_icon_;
  convertToGray(gray_icon, gray_lightness);
  p_gray_icon_->setPixmap(QPixmap::fromImage(gray_icon));
}

void MainWindow::convertToGray(QImage &image, int lighten)
{
  const int image_width  = image.width();
  const int image_height = image.height();
  for(int y=0; y<image_height; y++)
  {
    QCoreApplication::processEvents();
    for(int x=0; x<image_width; x++)
    {
      QRgb pix = image.pixel(x, y);
      int r = qRed(pix);
      int g = qGreen(pix);
      int b = qBlue(pix);
      int a = qAlpha(pix);

      // transparent
      if(!r && !g && !b && !a)
        continue;

      r = g = b = (qGray(pix) + lighten);
      correctRgba(r, g, b, a);
      image.setPixel(x, y, qRgba(r, g, b, a));
    }
  }
}

void MainWindow::adjustLightness(QImage &image, int lr, int lg, int lb, int la)
{
  const int image_width  = image.width();
  const int image_height = image.height();
  for(int y=0; y<image_height; y++)
  {
    QCoreApplication::processEvents();
    for(int x=0; x<image_width; x++)
    {
      QRgb pix = image.pixel(x, y);
      int r = qRed(pix);
      int g = qGreen(pix);
      int b = qBlue(pix);
      int a = qAlpha(pix);

      // transparent
      if(!r && !g && !b && !a)
        continue;

      r += lr;
      g += lg;
      b += lb;
      a += la;
      correctRgba(r, g, b, a);
      image.setPixel(x, y, qRgba(r, g, b, a));
    }
  }
}

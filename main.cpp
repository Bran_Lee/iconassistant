#include <QApplication>
#include <QTranslator>
#include "main_window.h"
#include "global_values.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QTranslator qt_translator;
  qt_translator.load(":/translations/qt_zh_CN.qm");
  app.installTranslator(&qt_translator);

  QTranslator app_translator;
  app_translator.load(":/translations/app_zh_CN.qm");
  app.installTranslator(&app_translator);

  app.setWindowIcon(QIcon(":/resources/main.png"));
  app.setApplicationName(global_app_name);
  app.addLibraryPath(app.applicationDirPath() + "/plugins");

  if(!initGlobals())
    return -1;

  MainWindow win;
  win.show();
  int exit = app.exec();

  unInitGlobals();
  return exit;
}

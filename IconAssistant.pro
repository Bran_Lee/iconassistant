#-------------------------------------------------
#
# Project created by QtCreator 2015-04-09T14:51:42
#
#-------------------------------------------------

QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = IconAssistant
TEMPLATE = app

HEADERS  += main_window.h \
            global_values.h

SOURCES += main.cpp \
           main_window.cpp \
           global_values.cpp

TRANSLATIONS = translations/app_zh_CN.ts

RESOURCES += resources.qrc

RC_FILE  = resources/main.rc
RC_ICONS = resources/main.ico

